﻿<p>
    &nbsp;</p>
<table border="0" cellpadding="0" cellspacing="0" style="width: 650px">
    <tbody>
        <tr>
            <td colspan="4">
                <p>
                    <span style="font-size:16px;"><strong>Tools</strong></span></p>
            </td>
        </tr>
        <tr>
            <td style="width: 120px;">
                <p style="text-align: center;">
                    <a href="http://en.kan-therm.com/kan/upload/narzedzia-do-montazu-PUSH-instrukcja-kan-therm-pl-en-ru.pdf"><img alt="" class="user-manual" src="/kan/upload/narzedzia-do-montazu-PUSH-instrukcja-kan-therm-pl-en-ru.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: #fff;" /><br />
					<br />
					<span style="font-size:10px;">Set of tools for assembly KAN-therm Push / Push Platinum</span></a></p>
            </td>
            <td>
                <p style="text-align: center;">
                    <a href="http://en.kan-therm.com/kan/upload/narzedzia-praska-i-rozpierak-instrukcja-kan-therm-pl.pdf"><img alt="" class="user-manual" src="/kan/upload/narzedzia-praska-i-rozpierak-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
					<br />
					<span style="font-size:10px;">Expander and Pusher Tool</span><br />
					<span style="font-size:10px;">Novopress AAP102</span><br />
					<span style="font-size:10px;">, AXI102</span></a></p>
            </td>
            <td style="text-align: center;">
                <a href="http://en.kan-therm.com/kan/upload/narzedzia-zaciskarka-rems-power-press-instrukcja-kan-therm-pl.pdf"><img alt="" class="user-manual" src="/kan/upload/narzedzia-zaciskarka-rems-power-press-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
				<br />
				<span style="font-size:10px;">Rems Power Press<br />
				Drive Unit (ZAPR01)<br />
				Accu Press (ZAPRAK)</span></a></td>
            <td style="text-align: center;">
                <a href="http://en.kan-therm.com/kan/upload/narzedzia-zaciskarka-novopress-AFP101-instrukcja-kan-therm-pl.pdf"><img alt="" class="user-manual" src="/kan/upload/narzedzia-zaciskarka-novopress-AFP101-instrukcja-kan-therm-pl-en-ru.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
				<br />
				<span style="font-size:10px;">Press machine<br />
				Novopress AFP101</span></a></td>
        </tr>
        <tr>
            <td style="width: 120px;">
                <p style="text-align: center;">
                    <a href="http://en.kan-therm.com/kan/upload/novopress-eco301.pdf"><img alt="" class="user-manual" src="/kan/upload/narzedzia-zaciskarka-novopress-ECO-301-instrukcja-kan-therm-pl-en-ru.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
					<br />
					<span style="font-size:10px;">Press machine<br />
					Novopress ECO 301</span></a></p>
            </td>
            <td>
                <p style="text-align: center;">
                    <a href="http://en.kan-therm.com/kan/upload/novopress-aco401.pdf"><img alt="" class="user-manual" src="/kan/upload/narzedzia-zaciskarka-novopress-ACO-401-instrukcja-kan-therm-pl-en-ru.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
					<br />
					<span style="font-size:10px;">Press machine<br />
					Novopress ACO 401</span></a></p>
            </td>
            <td style="text-align: center;">
                <a href="http://en.kan-therm.com/kan/upload/Klauke%20UAP100.pdf"><img alt="" class="user-manual" src="/kan/upload/narzedzia-zaciskarka-klauke-UAP-100-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
				<br />
				<span style="font-size:10px;">Press machine<br />
				Klauke UAP 100</span></a></td>
            <td style="text-align: center;">
                <a href="http://en.kan-therm.com/kan/upload/instrukcja-do-narzedzi-v2.pdf"><img alt="" class="user-manual" src="/kan/upload/narzedzia-konserwacja-szczek-zaciskowych-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
				<br />
				<span style="font-size:10px;">Jaws maintenance</span></a></td>
        </tr>
        <tr>
            <td>
                <p style="text-align: center;">
                    <a href="http://en.kan-therm.com/kan/upload/rems-cento.pdf"><img alt="" class="user-manual" src="/kan/upload/narzedzia-obcinarka-do-rur-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
					<br />
					<span style="font-size:10px;">Pipe cutter<br />
					Rems Cento</span></a></p>
            </td>
            <td>
                <p style="text-align: center;">
                    <a href="http://en.kan-therm.com/kan/upload/instrukcja-tacker.pdf"><img alt="" class="user-manual" src="/kan/upload/narzedzia-tacker-do-spinek-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
					<br />
					<span style="font-size:10px;">Tacker for clips</span></a></p>
            </td>
            <td style="text-align: center;">
                <a href="http://en.kan-therm.com/kan/upload/KAN-Instrukcja-Uniwersalny-Rozwijak-do-rur-PL-RU-EN-DE.pdf"><img alt="" class="user-manual" src="/kan/upload/narzedzia-uniwersalny-rozwijak-do-rur-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
				<br />
				<span style="font-size:10px;">Universal<br />
				pipe decoiler</span></a></td>
            <td style="text-align: center;">
                <a href="/kan/upload/plastic-tacker-instrukcja-kan-therm-pl-en-ru.pdf"><span style="font-size: 10px;"><img alt="" class="user-manual" src="/kan/upload/tacker(1).jpg" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /></span><br />
				<br />
				<span style="font-size: 10px;">Plastic Tacker</span></a></td>
        </tr>
        <tr>
            <td>
                <p>
                    <br />
                    <span style="font-size:16px;"><strong>Manifolds</strong></span></p>
            </td>
            <td>
                <p style="text-align: center;">
                    &nbsp;</p>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <p style="text-align: center;">
                    <a href="http://en.kan-therm.com/kan/upload/rozdzielacze-s10-s20.pdf"><img alt="" class="user-manual" src="/kan/upload/rozdzielacze-S10-S20-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
					<span style="font-size:10px;">Manifolds S10 and S20</span></a></p>
            </td>
            <td>
                <p style="text-align: center;">
                    <a href="http://en.kan-therm.com/kan/upload/rozdzielacze-61-74-81.pdf"><span style="font-size:10px;"><img alt="" class="user-manual" src="/kan/upload/rozdzielacze-61-74-81-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
					Manifolds 61, 74, 81</span></a></p>
            </td>
            <td style="text-align: center;">
                <a href="http://en.kan-therm.com/kan/upload/KAN-Instrukcja-rozdzielaczy-serii-91.pdf"><span style="font-size:10px;"><img alt="" class="user-manual" src="/kan/upload/rozdzielacze-91-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
				Manifolds 91</span></a></td>
            <td style="text-align: center;">
                <a href="/kan/upload/manifold-user-manual-73-77-3in1.pdf"><span style="font-size: 10px;"><img alt="" class="user-manual" src="/kan/upload/rozdzielacze-73A-77A-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
				M</span></a><span style="font-size: 10px;"><a href="/kan/upload/manifold-user-manual-73-77-3in1.pdf">anifolds 73&nbsp;and 77</a></span><br /> &nbsp;
            </td>
            <td style="text-align: center;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center;">
                <a href="http://en.kan-therm.com/kan/upload/INSTR-RN-PL-instrukcja-N75A-28-04-2015-druk-pokrzywionyx.pdf" style="text-align: center;"><span style="font-size: 10px;"><img alt="" class="user-manual" src="/kan/upload/rozdzielacze-N75A-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
				Manifolds N75A</span></a></td>
            <td style="text-align: center;">
                <a href="/kan/upload/manifolds-manual-71A-75A-51A-55A.PDF" style="text-align: center;"><span style="font-size: 10px;"><img alt="" class="user-manual" src="/kan/upload/rozdzielacz-71a75a51a-55a.jpg" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
				Manifolds&nbsp;</span><span style="font-size: 10px;">71A, 75A, 51A, 55A</span></a></td>
            <td style="text-align: center;">
                &nbsp;</td>
            <td style="text-align: center;">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td style="text-align: center;">
                &nbsp;</td>
            <td style="text-align: center;">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <br />
                <span style="font-size:16px;"><strong>Pump mixing units and accessories</strong></span><br /> &nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: center;">
                <p style="margin: 10px 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Verdana, sans-serif; vertical-align: baseline; color: rgb(80, 88, 92); background-color: rgb(247, 247, 247);">
                    <a href="http://en.kan-therm.com/kan/upload/instrukcja-grup-pompowych-K803000-K803001-K803002-PL-EN-RU.PDF" style="margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; color: rgb(113, 113, 113); text-decoration-line: none;"><img alt="Instrukcje" class="user-manual" src="http://pl.kan-therm.com/kan/upload/grupy-pomopowe.jpg" style="margin: 0px; padding: 0px; border: 7px solid white; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; vertical-align: baseline; width: 120px; height: 120px;" title="Instrukcje" /></a></p>
                <p style="margin: 10px 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Verdana, sans-serif; vertical-align: baseline; color: rgb(80, 88, 92); background-color: rgb(247, 247, 247);">
                    <a href="http://en.kan-therm.com/kan/upload/instrukcja-grup-pompowych-K803000-K803001-K803002-PL-EN-RU.PDF"><span style="margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 10px; line-height: inherit; font-family: inherit; vertical-align: baseline;"><font color="#717171" face="inherit"><span style="border-style: initial; border-color: initial; border-image: initial; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit;">Pump group manual</span></font><br />
					<font color="#717171" face="inherit"><span style="border-style: initial; border-color: initial; border-image: initial; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit;">K-803000 K803001 K803002</span></font></span></a></p>
            </td>
            <td style="text-align: center;">
                <p style="margin: 10px 0px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; line-height: inherit; font-family: Verdana, sans-serif; vertical-align: baseline; color: rgb(80, 88, 92); background-color: rgb(247, 247, 247);">
                    <a href="/kan/upload/glowica-termostatyczna-z-czujnikiem-przylgowym-instrukcja-kan-therm-pl.PDF" style="font-size: 10px;"><img alt="" class="user-manual" src="/kan/upload/instrukcja-glowica.jpg" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
					<br />
					Temperature controller<br />
					with contact sensor</a></p>
            </td>
            <td style="text-align: center;">
                &nbsp;</td>
            <td style="text-align: center;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center;">
                &nbsp;</td>
            <td style="text-align: center;">
                &nbsp;</td>
            <td style="text-align: center;">
                &nbsp;</td>
            <td style="text-align: center;">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <p>
                    <br />
                    <br />
                    <span style="font-size:16px;"><strong>System KAN-therm SMART</strong></span></p>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <p style="text-align: center;">
                    <a href="http://en.kan-therm.com/kan/upload/Silownik_SMART_24V.pdf"><span style="font-size:10px;"><img alt="" class="user-manual" src="/kan/upload/smart-silownik-24V-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
					<br />
					Servomotor SMART 24V</span></a></p>
            </td>
            <td>
                <p style="text-align: center;">
                    <a href="http://en.kan-therm.com/kan/upload/Silownik_SMART_230V.pdf"><span style="font-size:10px;"><img alt="" class="user-manual" src="/kan/upload/smart-silownik-230V-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
					<br />
					Servomotor SMART 230V</span></a></p>
            </td>
            <td style="text-align: center;">
                <a href="http://en.kan-therm.com/kan/upload/listwa-kan-therm-smart.pdf"><span style="font-size:10px;"><img alt="" class="user-manual" src="/kan/upload/smart-listwa-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
				<br />
				Strip KAN-therm SMART<br />
				230/24V</span></a><br /> &nbsp;
            </td>
            <td style="text-align: center;">
                <a href="http://www.ezr-home.de/images/ezr/doc/127266_1416_RBG_Funk_Display_SP1_w.pdf"><span style="font-size:10px;"><img alt="" class="user-manual" src="/kan/upload/smart-termostat-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
				<br />
				Thermostat KAN-therm SMART</span></a></td>
        </tr>
        <tr>
            <td>
                <p style="text-align: center;">
                    <a href="http://en.kan-therm.com/kan/upload/Antena_zewnetrzna_SMART_PL_ENG_DE.pdf"><span style="font-size:10px;"><img alt="" class="user-manual" src="/kan/upload/smart-antena-zew-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
					<br />
					External aerial<br />
					KAN-therm SMART</span></a></p>
            </td>
            <td>
                <p style="text-align: center;">
                    <a href="http://en.kan-therm.com/kan/upload/Repeater_SMART_PL_ENG.pdf"><span style="font-size:10px;"><img alt="" class="user-manual" src="/kan/upload/smart-przekaznik-sygnalu-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
					<br />
					Signal transmitter (Repeater)<br />
					KAN-therm SMART</span></a></p>
            </td>
            <td style="text-align: center;">
                <a href="http://www.ezr-home.de/images/ezr/doc/128028_1438_EZR_Manager_eng_w.pdf"><span style="font-size:10px;"><img alt="" class="user-manual" src="/kan/upload/smart-EZR-manager-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
				<br />
				EZR-Manager (SMART<br />
				management automation )</span></a></td>
            <td style="text-align: center;">
                <p>
                    <a href="http://en.kan-therm.com/kan/upload/smart-control-website-en.pdf"><img alt="" class="user-manual" src="/kan/upload/qa.JPG" style="width: 120px; height: 120px; border-width: 7px; border-style: solid; border-color: white;" /></a></p>
                <p>
                    <span style="font-size:10px;"><a href="http://en.kan-therm.com/kan/upload/smart-control-website-en.pdf">KAN Smart Control<br />
					Mobile App</a></span></p>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <p>
                    <br />
                    <br />
                    <span style="font-size:16px;"><strong>System KAN-therm BASIC+</strong></span></p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="text-align: center;">
                    <a href="http://en.kan-therm.com/kan/upload/Silownik_SMART_24V.pdf"><span style="font-size:10px;"><img alt="" class="user-manual" src="/kan/upload/system-BASIC-silownik-24V-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
					<br />
					Servomotor SMART 24V</span></a></p>
            </td>
            <td>
                <p style="text-align: center;">
                    <a href="http://en.kan-therm.com/kan/upload/Silownik_SMART_230V.pdf"><span style="font-size:10px;"><img alt="" class="user-manual" src="/kan/upload/system-BASIC-silownik-24V-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
					<br />
					Servomotor SMART 230V</span></a></p>
            </td>
            <td style="text-align: center;">
                <a href="http://en.kan-therm.com/kan/upload/instrukcjaobslugitermostatyLCD.pdf"><span style="font-size:10px;"><img alt="" class="user-manual" src="/kan/upload/system-BASIC-termostat-obsluga-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
				<br />
				Thermostat Basic+<br />
				with LCD 230V/24V<br />
				(Standard/Control)<br />
				- use</span></a></td>
            <td style="text-align: center;">
                <a href="http://en.kan-therm.com/kan/upload/instrukcjamontazutermostatyLCD.pdf"><span style="font-size:10px;"><img alt="" class="user-manual" src="/kan/upload/system-BASIC-termostat-montaz-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
				<br />
				Termostat Basic+<br />
				with LCD 230V/24V<br />
				(Standard/Control)<br />
				- installation</span></a></td>
        </tr>
        <tr>
            <td>
                <p style="text-align: center;">
                    <a href="http://en.kan-therm.com/kan/upload/K-800214_218analog230V.pdf"><span style="font-size:10px;"><img alt="" class="user-manual" src="/kan/upload/system-BASIC-termostat-analogowy-230V-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
					<br />
					Analogue thermostat<br />
					Basic+ 230V </span></a></p>
            </td>
            <td>
                <p style="text-align: center;">
                    <a href="http://en.kan-therm.com/kan/upload/K-800212_216analog24V.pdf"><span style="font-size:10px;"><img alt="" class="user-manual" src="/kan/upload/system-BASIC-termostat-analogowy-24V-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
					<br />
					Analogue thermostat<br />
					Basic+ 24V )</span></a></p>
            </td>
            <td style="text-align: center;">
                <a href="http://en.kan-therm.com/kan/upload/K-800224_226_228_230LISTWY.pdf"><span style="font-size:10px;"><img alt="" class="user-manual" src="/kan/upload/system-BASIC-listwa-24V-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
				<br />
				Strip Basic+ 230/24V<br />
				(heating/cooling<br />
				&ndash; 6zones/10zones)</span></a></td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <p>
                    <br />
                    <br />
                    <span style="font-size:16px;"><strong>KAN-therm BASIC Automation System</strong></span></p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="text-align: center;">
                    <a href="http://en.kan-therm.com/kan/upload/user-manual-basic-230-24v-en.pdf"><span style="font-size:10px;"><img alt="" class="user-manual" src="http://en.kan-therm.com/kan/upload/automatyka-BASIC-listwa-instrukcja-kan-therm-pl.PNG" style="text-align: center; height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
					<br />
					Strip KAN-therm Basic 230/24V<br />
					(withdrawn from offer)</span></a></p>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <p>
                    <br />
                    <br />
                    <strong><span style="font-size:16px;">Other equipment and accessories</span></strong></p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="text-align: center;">
                    <a href="http://en.kan-therm.com/kan/upload/KAN-Zaw%C3%B3r-Ogrodowy-Instrukcja-A5-DE-EN-PL-RU-L-500.pdf"><span style="font-size:10px;"><img alt="" class="user-manual" src="/kan/upload/armatura-i-komponenty-mrozoodporny-zawor-ogrodowy-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
					Frost-free garden tap AQS-DN15</span></a></p>
            </td>
            <td>
                <p style="text-align: center;">
                    <a href="http://en.kan-therm.com/kan/upload/instrukcja-montaz-srubunkow.pdf"><span style="font-size:10px;"><img alt="" class="user-manual" src="/kan/upload/armatura-i-komponenty-montaz-srubunkow-instrukcja-kan-therm-pl.PNG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
					Fittings installation manual</span></a></p>
            </td>
            <td style="text-align: center;">
                &nbsp;</td>
            <td style="text-align: center;">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <p style="text-align: center;">
                    &nbsp;</p>
            </td>
            <td>
                <p style="text-align: center;">
                    &nbsp;</p>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <p>
                    <br />
                    <br />
                    <strong style="font-size: 16px;">Cabinets and installation boxes</strong></p>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;">
                <a href="http://en.kan-therm.com/kan/upload/KAN-INSTRUKCJA-SZAFKI-PL-23-07-2014.pdf"><img alt="" class="user-manual" src="/kan/upload/szafki-i-obudowy-instrukcja-kan-therm-pl.PNG" style="font-size: 10px; text-align: center; height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br style="font-size: 10px; text-align: center;" />
				<span style="font-size: 10px;">Cabinets installation manual</span></a></td>
            <td>
                <p style="text-align: center;">
                    <a href="http://pl.kan-therm.com/kan/upload/szachty-instrukcja-kan-therm-uni.pdf"><span style="font-size:10px;"><img alt="" class="user-manual" src="http://pl.kan-therm.com/kan/upload/szachty.JPG" style="height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br />
					Residential manifolds unit</span></a></p>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>

        <tr>
            <td colspan="4">
                <p>
                    <br />
                    <br />
                    <strong style="font-size: 16px;">Concrete Additive</strong></p>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;">
                <a href="../kan/upload/_instrukcje/20180423/betokan-5kg-komplet.pdf#page=4">
                <img alt="" class="user-manual" src="http://pl.kan-therm.com/kan/upload/ogrzewanie-plaszczyznowe-BetoKAN-instrukcja-kan-therm-pl.PNG" style="font-size: 10px; text-align: center; height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br style="font-size: 10px; text-align: center;" />
				<span style="font-size: 10px;">BETOKAN 0.1004 5Kg<br>Concrete additive reducing needed water volume.</span></a></td>
            <td>
                <p style="text-align: center;">

                    <a href="../kan/upload/_instrukcje/20180423/betokan-10kg-komplet.pdf#page=4">
                    <img alt="" class="user-manual" src="http://pl.kan-therm.com/kan/upload/ogrzewanie-plaszczyznowe-BetoKAN-instrukcja-kan-therm-pl.PNG" style="font-size: 10px; text-align: center; height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br style="font-size: 10px; text-align: center;" />
                    <span style="font-size: 10px;">BETOKAN 0.1005 10Kg<br>Concrete additive reducing needed<br> water volume.</span></a></td>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>


    </tbody>
</table>
<p>
    &nbsp;</p>
<table border="0" cellpadding="0" cellspacing="0" style="width: 450px;">
    <tbody>
        <tr>
            <td colspan="4">
                <p>
                    <strong style="font-size: 16px;">Other</strong></p>
            </td>
        </tr>
        <tr>
            <td style="width: 170px; text-align: center;">
                <a href="/kan/upload/transport_storage_of_PP_pipes-.pdf"><img alt="" class="user-manual" src="/kan/upload/instrukcja.jpg" style="font-size: 10px; height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br style="font-size: 10px;" />
				<span style="font-size:10px;">Transport and storage instruction<br />
				of KAN-therm&nbsp;</span><span style="font-size:10px;">PP System</span></a>
            </td>

            <td style="width: 170px; text-align: center;">
                <a href="/kan/upload/instrukcja-kompenastory-osiowe-A5-PL-RU-EN-DE-2018(1).pdf"><img alt="" class="user-manual" src="/kan/upload/kompensator-mieszkowy.jpg" style="font-size: 10px; height: 120px; width: 120px; border-width: 7px; border-style: solid; border-color: rgb(255, 255, 255);" /><br style="font-size: 10px;" />
				<span style="font-size:9px;">Axial compensators<br />
				</a>
            </td>

            <td style="text-align: center;">
                &nbsp;</td>
            <td style="text-align: center;">
                &nbsp;</td>
    </tbody>
</table>
<p>
    &nbsp;</p>
